name 'elasticsearch-platform'
maintainer 'Richard Delaplace'
maintainer_email 'rdelaplace@yueyehua.net'
license 'Apache 2.0'
description 'Installs/Configures elasticsearch-platform'
long_description 'Installs/Configures elasticsearch-platform'
source_url 'https://gitlab.com/rdelaplace/elasticsearch-platform'
issues_url 'https://gitlab.com/rdelaplace/elasticsearch-platform/issues'
version '1.0.0'

supports 'centos', '>= 7.1'

depends 'cluster-search'
