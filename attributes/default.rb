#
# Copyright (c) 2016 Richard Delaplace
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Install the dependancies
default['elasticsearch-platform']['deps'] = {
  'centos' => [
    'tar',
    'java-1.8.0-openjdk'
  ],
  'debian' => [
    'tar',
    'openjdk-8-jdk',
    'apt-transport-https'
  ]
}

# Set Elasticsearch installation parameters
default['elasticsearch-platform']['distrib'] = 'centos'
default['elasticsearch-platform']['version'] = '5.1.1-1'
default['elasticsearch-platform']['repo_version'] = '5.x'
default['elasticsearch-platform']['repo_file'] =
  '/etc/yum.repos.d/elastic.repo'

# Zookeeper configuration
default['elasticsearch-platform']['zookeeper']['role'] =
  'elasticsearch-platform'
default['elasticsearch-platform']['zookeeper']['hosts'] = []
default['elasticsearch-platform']['zookeeper']['size'] = 1

default['elasticsearch-platform']['config'] = {
  'elasticsearch.local.dir' => '/var/opt/elasticsearch/lib'
}

# Set initiator id
default['elasticsearch-platform']['initiator_id'] = 1

# Elasticsearch configuration
default['elasticsearch-platform']['node_base_name'] = 'NODE'
default['elasticsearch-platform']['elasticsearch.yml']['cluster.name'] =
  'elasticsearch'

default['elasticsearch-platform']['plugins'] = [
  'analysis-icu',
  'discovery-file'
]
