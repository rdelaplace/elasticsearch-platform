#
# Cookbook:: elasticsearch-platform
# Recipe:: install
#
# Copyright:: 2016, Richard Delaplace
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Install dependencies
node[cookbook_name]['deps'][node[cookbook_name]['distrib']].each do |pkg|
  package pkg
end

# Add Elasticsearch repository
template node[cookbook_name]['repo_file'] do
  source "elasticsearch.repo.#{node[cookbook_name]['distrib']}.erb"
  mode '0644'
  owner 'root'
  group 'root'
  variables version: node[cookbook_name]['repo_version']
end

package 'elasticsearch' do
  action :install
  version node[cookbook_name]['version']
end
