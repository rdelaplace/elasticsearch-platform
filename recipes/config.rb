#
# Cookbook:: elasticsearch-platform
# Recipe:: config
#
# Copyright:: 2016, Richard Delaplace
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Search for Zookeeper
::Chef::Recipe.send(:include, ClusterSearch)
zookeeper_cluster = cluster_search(node[cookbook_name]['zookeeper'])
return if zookeeper_cluster.nil?

# Get other elasticsearch nodes
config = node[cookbook_name]['config'].to_hash
config['elasticsearch.zookeeper.servers'] = zookeeper_cluster['hosts']
elasticsearch_cluster = cluster_search(node[cookbook_name])
return if elasticsearch_cluster.nil?

init_id = node[cookbook_name]['initiator_id']
node_id = elasticsearch_cluster['my_id']
if init_id < 1 || init_id > elasticsearch_cluster['hosts'].size
  raise "Invalid nimbus range (#{range}) for a #{cluster_size} nodes cluster"
end
raise 'Cannot find myself in the cluster' if node_id == -1

# Set Elasticsearch configuration
node.default[cookbook_name]['elasticsearch.yml']['node.name'] =
  "#{node['elasticsearch-platform']['node_base_name']}-#{node_id}"
template '/etc/elasticsearch/elasticsearch.yml' do
  source 'elasticsearch.yml.erb'
  mode '0660'
  owner 'root'
  group 'elasticsearch'
  variables 'config' => node[cookbook_name]['elasticsearch.yml']
end
