Elasticsearch-platform
======================

Description
-----------

[Elasticsearch](https://www.elastic.co/products/elasticsearch) is a
distributed, RESTful search and analytics engine capable of solving a growing
number of use cases. As the heart of the Elastic Stack, it centrally stores
your data so you can discover the expected and uncover the unexpected.

This cookbooks installs and configures Elasticsearch (> 5.1.1).

Requirements
------------

### Cookbooks and gems

Declared in [metadata.rb](metadata.rb) and in [Gemfile](Gemfile).

### Platforms

A *systemd* managed distribution:
- RHEL Family 7, tested on Centos

Note: it should work quite fine on Debian 8 (with some attributes tuning) but
the official docker image does not allow Systemd to work easily, so it could
not be tested.

Usage
-----

This cookbook may be used as is and only need to be added in your run list.
Attributes may be overwritten according to your needs.

Attributes
----------

Configuration is done by overriding default attributes. All configuration keys
have a default defined in [attributes/default.rb](attributes/default.rb).
Please read it to have a comprehensive view of what and how you can configure
this cookbook behavior.

Recipes
-------

### default

Install, configure and start Elasticsearch and its plugins.

### install

Install Elasticsearch from the official repository.

### config

Configure Elasticsearch depending on the configurations.

### plugins

Install all the plugins selected in the attribute.

### service

Enable and start Elasticsearch service.

Changelog
---------

Available in [CHANGELOG.md](CHANGELOG.md).

Contributing
------------

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

Acknowledgement
---------------

Available in [THANKS.md](THANKS.md)

License and Author
------------------

- Author:: Richard Delaplace (<rdelaplace@yueyehua.net)

```text
Copyright (c) 2016 Richard Delaplace

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

[cluster-search]: https://supermarket.chef.io/cookbooks/cluster-search
[zookeeper-platform]: https://supermarket.chef.io/cookbooks/zookeeper-platform
